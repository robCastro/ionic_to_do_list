import { Component } from '@angular/core';
import { DeseosService } from 'src/app/services/deseos.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Lista } from 'src/app/models/lista.model';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(
    public deseosService: DeseosService,
    private router: Router,
    private alertController: AlertController
  ) {}

  private URL_DETALLE_LISTA:string = '/tabs/tab1/agregar/';
  public async agregarLista() {

    
    const alert = await this.alertController.create({
      header: 'Nueva Lista',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Crear',
          handler: (data) => {
            if(data.titulo.length > 0){
              const listaId = this.deseosService.crearLista(data.titulo);
              this.router.navigateByUrl(this.URL_DETALLE_LISTA + listaId);
            }
          }
        }
      ],
      inputs: [
        {
          name: 'titulo',
          type: 'text',
          placeholder: 'Nombre de la Lista',
        }
      ]
    });

    alert.present();
  }

  /* public listaSeleccionada(lista: Lista){
    this.router.navigateByUrl(this.URL_DETALLE_LISTA + lista.id);
  } */
}
