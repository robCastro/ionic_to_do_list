import { Injectable } from '@angular/core';
import { Lista } from '../models/lista.model';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {
  listas: Lista[] = [];
  
  constructor() { 
    this.cargarStorage();
  }

  public crearLista(titulo: string){
    const lista = new Lista(titulo)
    this.listas.push(lista);
    this.guardarStorage();
    return lista.id;
  }

  public obtenerLista(id:number|string){
    id = Number(id);
    return this.listas.find( listaData => listaData.id === id )
  }

  guardarStorage(){
    localStorage.setItem('data', JSON.stringify(this.listas));
  }

  cargarStorage(){
    if (localStorage.getItem('data'))
      this.listas = JSON.parse(localStorage.getItem('data'));
    else{
      this.listas = [];
    }
  }

  public eliminarLista(lista: Lista){
    this.listas = this.listas.filter(l => l.id !== lista.id);
    this.guardarStorage();
  }

  public editarLista(lista: Lista){
    const posicion = this.listas.findIndex(l => l.id === lista.id);
    this.listas.splice(posicion, 1, lista);
    this.guardarStorage();
  }
}
