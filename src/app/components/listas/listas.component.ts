import { Component, OnInit, Input } from '@angular/core';
import { DeseosService } from 'src/app/services/deseos.service';
import { Router } from '@angular/router';
import { Lista } from 'src/app/models/lista.model';
import { AlertController, IonItemSliding } from '@ionic/angular';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss'],
})
export class ListasComponent implements OnInit {

  @Input() terminada:boolean = true;

  constructor(

    public deseosService: DeseosService,
    private router: Router,
    private alertController: AlertController

  ) { }

  ngOnInit() {}

  public listaSeleccionada(lista: Lista){
    if(this.terminada){
      this.router.navigateByUrl(`/tabs/tab2/agregar/${lista.id}`);
    }
    else{
      this.router.navigateByUrl(`/tabs/tab1/agregar/${lista.id}`);
    }
  }

  public borrarLista(lista: Lista){
    this.deseosService.eliminarLista(lista);
  }

  public async editarLista(lista: Lista, sliding: IonItemSliding){
    console.log(sliding);
    const alert = await this.alertController.create({
      header: 'Editar',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            sliding.close();
          }
        },
        {
          text: 'Editar',
          handler: (data) => {
            if(data.titulo.length > 0){
              lista.titulo = data.titulo;
              // this.deseosService.editarLista(lista);
              this.deseosService.guardarStorage();
              sliding.close();
            }
          }
        }
      ],
      inputs: [
        {
          name: 'titulo',
          type: 'text',
          value: lista.titulo,
          placeholder: 'Nombre de la Lista',
        }
      ]
    });
    alert.present();
  }
}
